package br.rio.puc.lac.sddlclient.message;

import java.util.ArrayList;

import lac.cnclib.sddl.message.Message;

/**
 * Created by Malone on 9/22/2014 5:32 PM
 */
public class MessageManager {

    private static ArrayList<Message> messageList = new ArrayList<Message>();
    private static ArrayList<String> responseList = new ArrayList<String>();

    /**
     * Tell if there are pending messages
     *
     * @return true if there are pending messages
     */
    public static Boolean hasPendingMessages() {
        if (messageList.size() > 0)
            return true;

        return false;
    }

    /**
     * Consume the first pending message. Get the message and remove it from the pending messages list
     *
     * @return The current message to consume
     */
    public static Message consumeMessage() {
        Message message = messageList.get(0);
        messageList.remove(0);

        return message;
    }

    /**
     * Add a message to the list of pending messages
     *
     * @param message The message to add to the pending list
     */
    public static void addMessage(Message message) {
        messageList.add(message);
    }

    /**
     * Tell if there are pending responses
     *
     * @return true if there are pending responses
     */
    public static Boolean hasPendingResponses() {
        if (responseList.size() > 0)
            return true;

        return false;
    }

    /**
     * Consume the first pending response. Get the response and remove it from the pending responses list
     *
     * @return The current response to consume
     */
    public static String consumeResponse() {
        String response = responseList.get(0);
        responseList.remove(0);

        return response;
    }

    /**
     * Add a response to the list of pending responses
     *
     * @param response The response to add to the pending list
     */
    public static void addResponse(String response) {
        responseList.add(response);
    }
}
