package br.rio.puc.lac.sddlclient.service;

import android.content.Context;
import android.util.Log;

import java.net.SocketAddress;
import java.util.List;

import br.rio.puc.lac.sddlclient.config.AppConfig;
import br.rio.puc.lac.sddlclient.message.MessageManager;
import lac.cnclib.net.NodeConnection;
import lac.cnclib.net.NodeConnectionListener;
import lac.cnclib.sddl.message.Message;

/**
 * Created by Malone on 9/22/2014 12:49 PM
 */
public class ConnectionListener implements NodeConnectionListener {

    // Log TAG
    private final String TAG = "ConnectionListener";

    private Context context;

    public ConnectionListener(Context context) {
        this.context = context;
    }

    @Override
    public void connected(NodeConnection nodeConnection) {

        /*
        ApplicationMessage applicationMessage = new ApplicationMessage();
        applicationMessage.setContentObject("ack");
        applicationMessage.setTagList(new ArrayList<String>());
        applicationMessage.setSenderID(AppConfig.getUuid(context));

        try {
            nodeConnection.sendMessage(applicationMessage);
        } catch (IOException e) {
            e.printStackTrace();
        }
        */

        AppConfig.setWaitingToConnectSDDL(false);
        AppConfig.setIsSDDLConnected(context, true);

        Log.d(TAG, "Connected and Identified...");
    }

    @Override
    public void newMessageReceived(NodeConnection nodeConnection, Message message) {
        Log.d(TAG, "NewMessageReceived...");

        MessageManager.addResponse(message.getContentObject().toString());
    }

    @Override
    public void disconnected(NodeConnection nodeConnection) {
        Log.d(TAG, "Disconnected...");

        AppConfig.setIsSDDLConnected(context, false);
    }

    @Override
    public void internalException(NodeConnection nodeConnection, Exception e) {
        Log.d(TAG, "InternalException...");
    }

    @Override
    public void reconnected(NodeConnection nodeConnection, SocketAddress endPoint,
                            boolean wasHandover, boolean wasMandatory) {
        Log.d(TAG, "Reconnected...");
    }

    @Override
    public void unsentMessages(NodeConnection nodeConnection, List<Message> messageList) {
        Log.d(TAG, "UnsentMessages...");
    }
}
