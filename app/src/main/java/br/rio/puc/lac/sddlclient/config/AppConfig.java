package br.rio.puc.lac.sddlclient.config;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.UUID;

/**
 * Created by Malone on 9/22/2014 1:51 PM
 */
public class AppConfig {

    /* Config SharedPreferences filename */
    public static final String PREF_FILE = "SDDLClientConfig";
    public static final String PREF_VALUE_USER_UUID = "UserUUID";
    private static final String PREF_VALUE_BLUETOOTH_ADDRESS = "BluetoothAddress";
    public static final String PREF_VALUE_SDDL_SERVICE_RUNNING = "SDDLServiceRunning";
    public static final String PREF_VALUE_OBD2_SERVICE_RUNNING = "OBD2ServiceRunning";
    public static final String PREF_VALUE_IP_AND_PORT = "IpAndPort";
    public static final String PREF_VALUE_SDDL_IS_CONNECTED = "IsSDDLConnected";

    private static Boolean waitingToConnectSDDL = false;

    /**
     * Check if there is a valid IP/port stored on SharedPreferences
     *
     * @param context The application context
     * @return true if there is an IP/port stored
     */
    public static Boolean hasIp(Context context) {
        SharedPreferences config = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        String ip = config.getString(AppConfig.PREF_VALUE_IP_AND_PORT, "");
        if (!ip.isEmpty()) {
            return true;
        }

        saveIpPort( context, "lacobd.cloudapp.net:5500" );

        return false;
    }

    /**
     * Check if there is a valid bluetooth address stored on SharedPreferences
     *
     * @param context The application context
     * @return true if there is a bluetooth address stored
     */
    public static Boolean hasBluetoothAddress(Context context) {
        SharedPreferences config = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        String bluetoothAddress = config.getString(AppConfig.PREF_VALUE_BLUETOOTH_ADDRESS, "");
        if (!bluetoothAddress.isEmpty()) {
            return true;
        }

        setBluetoothAddress( context, "00:0D:18:3A:67:89" );

        return false;
    }

    /**
     * Generate a new UUID
     */
    public static UUID generateUuid() {
        return UUID.randomUUID();
    }

    /**
     * Save the UUID in the SharedPreferences file
     *
     * @param context The application context
     * @param uuid    The given UUID to save
     * @return true if successfully saved UUID
     */
    public static Boolean setUuid(Context context, UUID uuid) {
        SharedPreferences config = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor writer = config.edit();
        writer.putString(AppConfig.PREF_VALUE_USER_UUID, uuid.toString());
        return writer.commit();
    }

    /**
     * Get the store UUID from the user. If the UUID is not set, create one.
     *
     * @param context The application context
     * @return The user UUID
     */
    public static UUID getUuid(Context context) {

        SharedPreferences config = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        //String uuidRead = config.getString(AppConfig.PREF_VALUE_USER_UUID, "");
        String uuidRead = new String("b1f1f65c-6ae0-11e4-90a3-001cc01afd11");

        // No UUID store, need to create one
        if (uuidRead.equals("")) {

            UUID newId = generateUuid();

            if (setUuid(context, newId)) {
                return newId;
            }

            return null;

        } else {
            return UUID.fromString(uuidRead);
        }
    }

    /**
     * Get the status of the first connection.
     * True if already connected at least once. False if never connected.
     *
     * @return Status of the first connection
     */
    public static Boolean getWaitingToConnectSDDL() {
        return waitingToConnectSDDL;
    }

    /**
     * Set the status of the first connection
     *
     * @param flag Status to be set
     */
    public static void setWaitingToConnectSDDL(Boolean flag) {
        waitingToConnectSDDL = flag;
    }

    /**
     * Get the status of the SDDL service from the SharedPreferences
     *
     * @param context The application context
     * @return The flag of the SDDL service status
     */
    public static Boolean getSDDLService(Context context) {
        SharedPreferences config = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        Boolean service = config.getBoolean(AppConfig.PREF_VALUE_SDDL_SERVICE_RUNNING, false);
        return service;
    }

    /**
     * Save the SDDL service status to the SharedPreferences
     *
     * @param context The application context
     * @param flag    The flag of the SDDL service status
     * @return true if successfully saved the service status
     */
    public static Boolean setSDDLService(Context context, Boolean flag) {
        SharedPreferences config = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor writer = config.edit();
        writer.putBoolean(AppConfig.PREF_VALUE_SDDL_SERVICE_RUNNING, flag);
        return writer.commit();
    }

    /**
     * Get the SDDL connection status from the SharedPreferences
     *
     * @param context The application context
     * @return The flag of the SDDL connection status
     */
    public static Boolean getIsSDDLConnected(Context context) {
        SharedPreferences config = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        Boolean connected = config.getBoolean(AppConfig.PREF_VALUE_SDDL_IS_CONNECTED, false);
        return connected;
    }

    /**
     * Store the SDDL connection flag to the SharedPreferences
     *
     * @param context The application context
     * @param flag    The flag of the SDDL connection status
     * @return true if successfully saved the SDDL connection status
     */
    public static Boolean setIsSDDLConnected(Context context, Boolean flag) {
        SharedPreferences config = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor writer = config.edit();
        writer.putBoolean(AppConfig.PREF_VALUE_SDDL_IS_CONNECTED, flag);
        return writer.commit();
    }

    /**
     * Save the OBD2 service status to the SharedPreferences
     *
     * @param context The application context
     * @param flag    The flag of the OBD2 service status
     * @return true if successfully saved the service status
     */
    public static Boolean setOBD2Service(Context context, Boolean flag) {
        SharedPreferences config = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor writer = config.edit();
        writer.putBoolean(AppConfig.PREF_VALUE_OBD2_SERVICE_RUNNING, flag);
        return writer.commit();
    }

    /**
     * Get the status of the OBD2 service from the SharedPreferences
     *
     * @param context The application context
     * @return The flag of the OBD2 service status
     */
    public static Boolean getOBD2Service(Context context) {
        SharedPreferences config = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        Boolean service = config.getBoolean(AppConfig.PREF_VALUE_OBD2_SERVICE_RUNNING, false);
        return service;
    }

    /**
     * Get the ip from an ip/port string (IP:PORT)
     *
     * @param context The application context
     * @return The ip
     */
    public static String getIp(Context context) {
        SharedPreferences config = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        String ipAndPort = config.getString(AppConfig.PREF_VALUE_IP_AND_PORT, "");
        String[] result = ipAndPort.split(":");
        return result[0];
    }

    /**
     * Get the port from an ip/port string (IP:PORT)
     *
     * @param context The application context
     * @return The port number
     */
    public static int getPort(Context context) {
        SharedPreferences config = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        String ipAndPort = config.getString(AppConfig.PREF_VALUE_IP_AND_PORT, "");
        String[] result = ipAndPort.split(":");
        return Integer.parseInt(result[1]);
    }

    /**
     * Save the IP and port to SharedPreferences
     *
     * @param context   The application context
     * @param ipAndPort The ip and port to store (IP:PORT)
     */
    public static void saveIpPort(Context context, String ipAndPort) {
        SharedPreferences config = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor writer = config.edit();
        writer.putString(AppConfig.PREF_VALUE_IP_AND_PORT, ipAndPort);
        writer.commit();
    }

    /**
     * Get the ip and port from SharedPreferences
     *
     * @param context The application context
     * @return The ip and port stored (IP:PORT)
     */
    public static String getIpPort(Context context) {
        SharedPreferences config = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        String ipAndPort = config.getString(AppConfig.PREF_VALUE_IP_AND_PORT, "");

        if ( ipAndPort.isEmpty() ) {
            saveIpPort( context, "lacobd.cloudapp.net:5500" );
        }
        return ipAndPort;
    }


    /**
     * Get the bluetooth address from SharedPreferences
     *
     * @param context The application context
     * @return The bluetooth address
     */
    public static String getBluetoothAddress(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        String bluetoothAdapter = sharedPreferences.getString(AppConfig.PREF_VALUE_BLUETOOTH_ADDRESS, "");

        if ( bluetoothAdapter.isEmpty() ) {
            setBluetoothAddress( context, "00:0D:18:3A:67:89" );
        }

        return bluetoothAdapter;
    }

    /**
     * Save the bluetooth address to SharedPreferences
     *
     * @param context          The application context
     * @param bluetoothAddress The bluetooth address
     */
    public static void setBluetoothAddress(Context context, String bluetoothAddress) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(AppConfig.PREF_VALUE_BLUETOOTH_ADDRESS, bluetoothAddress);
        editor.commit();
    }
}
