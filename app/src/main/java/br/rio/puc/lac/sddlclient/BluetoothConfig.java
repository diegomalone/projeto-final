package br.rio.puc.lac.sddlclient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.rio.puc.lac.sddlclient.config.AppConfig;

/**
 * Created by Malone on 10/2/2014 11:11 AM
 */
public class BluetoothConfig extends Activity {

    private EditText etBluetoothAddress;
    private Button btnSave;
    private Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Set as dialog
        setTheme(android.R.style.Theme_Holo_Light_Dialog);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_config);

        etBluetoothAddress = (EditText) findViewById(R.id.etBluetoothAddress);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        String bluetoothAddress = AppConfig.getBluetoothAddress(getApplicationContext());
        etBluetoothAddress.setText(bluetoothAddress);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra(MainActivity.BLUETOOTH_ADDRESS, etBluetoothAddress.getText().toString());
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(RESULT_CANCELED, returnIntent);
                finish();
            }
        });

    }

}
