package br.rio.puc.lac.sddlclient.io;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import br.rio.puc.lac.sddlclient.MainActivity;
import br.rio.puc.lac.sddlclient.config.AppConfig;
import pt.lighthouselabs.obd.commands.protocol.EchoOffObdCommand;
import pt.lighthouselabs.obd.commands.protocol.LineFeedOffObdCommand;
import pt.lighthouselabs.obd.commands.protocol.SelectProtocolObdCommand;
import pt.lighthouselabs.obd.commands.protocol.TimeoutObdCommand;
import pt.lighthouselabs.obd.enums.ObdProtocols;

/**
 * Created by Malone on 10/2/2014 1:28 PM
 */
public class ObdGatewayService extends Service {

    private static final String TAG = ObdGatewayService.class.getName();

    private final IBinder mBinder = new ObdGatewayServiceBinder();

    private Context context;
    private boolean isRunning = false;
    private BlockingQueue<ObdCommandJob> jobsQueue = new LinkedBlockingQueue<ObdCommandJob>();
    protected boolean isQueueRunning = false;
    protected Long queueCounter = 0L;

    public class ObdGatewayServiceBinder extends Binder {
        public ObdGatewayService getService() {
            return ObdGatewayService.this;
        }
    }

    /*
     * http://developer.android.com/reference/android/bluetooth/BluetoothDevice.html
     * #createRfcommSocketToServiceRecord(java.util.UUID)
     *
     * "Hint: If you are connecting to a Bluetooth serial board then try using the
     * well-known SPP UUID 00001101-0000-1000-8000-00805F9B34FB. However if you
     * are connecting to an Android peer then please generate your own unique
     * UUID."
     */
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private BluetoothDevice bluetoothDevice = null;
    private BluetoothSocket bluetoothSocket = null;
    private BluetoothSocket bluetoothSocketFallback = null;

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void startService() {
        Log.d(TAG, "Starting service");

        String bluetoothAddress = AppConfig.getBluetoothAddress(context);
        if (bluetoothAddress == "" || bluetoothAddress.equals("")) {
            Toast.makeText(context, "No bluetooth device specified", Toast.LENGTH_LONG).show();

            stopService();
        }

        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if ( !bluetoothAdapter.isEnabled() ) {
            Toast.makeText( context, "Bluetooth disabled", Toast.LENGTH_LONG ).show();
        }

        bluetoothDevice = bluetoothAdapter.getRemoteDevice(bluetoothAddress);

        try {
            startObdConnection();
        } catch (Exception e) {
            Log.e(TAG, "There was an error while establishing connection. -> " + e.getMessage());

            // in case of failure, stop this service.
            stopService();
        }

        bluetoothAdapter.cancelDiscovery();
    }

    private void startObdConnection() throws IOException {
        Log.d(TAG, "Starting OBD2 Connection");

        try {
            bluetoothSocket = bluetoothDevice.createInsecureRfcommSocketToServiceRecord(MY_UUID);
            bluetoothSocket.connect();
        } catch (Exception e1) {
            Log.e(TAG, "Error establishing Bluetooth connection", e1);

            Class<?> clazz = bluetoothSocket.getRemoteDevice().getClass();
            Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};

            try {
                Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                Object[] params = new Object[]{Integer.valueOf(1)};

                bluetoothSocketFallback = (BluetoothSocket) m.invoke(bluetoothSocket.getRemoteDevice(), params);
                bluetoothSocketFallback.connect();

                bluetoothSocket = bluetoothSocketFallback;
            } catch (Exception e2) {
                Log.e(TAG, "Couldn't fallback while establishing Bluetooth connection", e2);
                stopService();
                return;
            }
        }

        try {
            new EchoOffObdCommand().run(bluetoothSocket.getInputStream(), bluetoothSocket.getOutputStream());
            new LineFeedOffObdCommand().run(bluetoothSocket.getInputStream(), bluetoothSocket.getOutputStream());
            new TimeoutObdCommand(62).run(bluetoothSocket.getInputStream(), bluetoothSocket.getOutputStream());
            new SelectProtocolObdCommand(ObdProtocols.AUTO).run(bluetoothSocket.getInputStream(), bluetoothSocket.getOutputStream());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        /*
        queueJob(new ObdCommandJob(new ObdResetCommand()));
        queueJob(new ObdCommandJob(new EchoOffObdCommand()));

        * Will send second-time based on tests.
        *
        * TODO this can be done w/o having to queue jobs by just issuing
        * command.run(), command.getResult() and validate the result.
        queueJob(new ObdCommandJob(new EchoOffObdCommand()));
        queueJob(new ObdCommandJob(new LineFeedOffObdCommand()));
        queueJob(new ObdCommandJob(new TimeoutObdCommand(62)));

        // For now set protocol to AUTO
        queueJob(new ObdCommandJob(new SelectProtocolObdCommand(ObdProtocols.AUTO)));

        // Job for returning dummy data
        queueJob(new ObdCommandJob(new AmbientAirTemperatureObdCommand()));
        */

        queueCounter = 0L;
        Log.d(TAG, "Initialization jobs queued");

        isRunning = true;

        AppConfig.setOBD2Service(getApplicationContext(), true);
    }

    public void queueJob(ObdCommandJob job) {
        queueCounter++;
        Log.d(TAG, "Adding job[" + queueCounter + "] to queue");

        job.setId(queueCounter);

        try {
            jobsQueue.put(job);
            Log.d(TAG, "Job queued");
        } catch (InterruptedException e) {
            job.setState(ObdCommandJob.ObdCommandJobState.QUEUE_ERROR);
            Log.e(TAG, "Failed to queue job");
        }

        if (!isQueueRunning) {
            // Run the executeQueue in a different thread to lighten the UI thread
            Thread t = new Thread(new Runnable() {

                @Override
                public void run() {
                    executeQueue();
                }
            });

            t.start();
        }
    }

    public void stopService() {
        Log.d(TAG, "Stopping service");

        jobsQueue.removeAll(jobsQueue); // TODO is this safe?
        isRunning = false;

        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }

        AppConfig.setOBD2Service(getApplicationContext(), false);

        // kill service
        stopSelf();
    }

    private void executeQueue() {
        Log.d(TAG, "Executing queue");

        isQueueRunning = true;

        while (!jobsQueue.isEmpty()) {
            ObdCommandJob job = null;

            try {
                job = jobsQueue.take();

                Log.d(TAG, "Taking job[" + job.getId() + "] from queue");

                if (job.getState().equals(ObdCommandJob.ObdCommandJobState.NEW)) {

                    job.setState(ObdCommandJob.ObdCommandJobState.RUNNING);
                    job.getCommand().run(bluetoothSocket.getInputStream(), bluetoothSocket.getOutputStream());
                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "Failed to run command -> " + e.getMessage());
                job.setState(ObdCommandJob.ObdCommandJobState.EXECUTION_ERROR);
            }

            if (job != null) {
                final ObdCommandJob jobUpdate = job;
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).stateUpdate(jobUpdate);
                    }
                });
            }
        }

        isQueueRunning = false;
    }

    public boolean isRunning() {
        return isRunning;
    }

}
