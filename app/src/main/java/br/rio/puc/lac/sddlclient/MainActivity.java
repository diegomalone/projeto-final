package br.rio.puc.lac.sddlclient;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import br.rio.puc.lac.sddlclient.config.AppConfig;
import br.rio.puc.lac.sddlclient.io.ObdCommandJob;
import br.rio.puc.lac.sddlclient.io.ObdGatewayService;
import br.rio.puc.lac.sddlclient.message.MessageManager;
import br.rio.puc.lac.sddlclient.service.ConnectionService;
import lac.cnclib.sddl.message.ApplicationMessage;
import lac.cnclib.sddl.message.ClientLibProtocol;
import pt.lighthouselabs.obd.commands.SpeedObdCommand;
import pt.lighthouselabs.obd.commands.engine.EngineRPMObdCommand;
import pt.lighthouselabs.obd.commands.temperature.EngineCoolantTemperatureObdCommand;
import pt.lighthouselabs.obd.enums.AvailableCommandNames;

/**
 * Created by Malone on 9/22/2014 12:49 PM
 */
public class MainActivity extends Activity {

    // Log TAG
    private final String TAG = "MainActivity";

    static final int CONFIGURE_IP_PORT = 1;
    private final int CONFIG_BLUETOOTH = 2;
    public static final String BLUETOOTH_ADDRESS = "bluetoothAddress";

    // Is pre requisites to start OBD2 service OK?
    private boolean preRequisites = true;

    // Is OBD2 Service bound?
    private boolean isOBD2ServiceBound = false;

    // OBD2 service
    private ObdGatewayService service;

    // Handler to process OBD2 commands
    private Handler mHandler = new Handler();

    // Handler to update the UI
    private final Handler uiHandler = new Handler();

    // Screen refresh rate
    int screenRefreshRate = 1000; // 1 second

    private Button btnStartService, btnStopService;
    private TextView tvGateway, tvSDDLService, tvSDDLConnected, tvOBD2Service, tvResponse;
    private Intent serviceIntent;

    private TextView rpmView, speedView;

    // Timer to update the UI
    private Timer uiTimer;
    private TimerTask uiTimerTask;

    // TODO Better counter
    private int counter = 1;

    final Runnable uiRunnable = new Runnable() {

        @Override
        public void run() {
            String ipPort = AppConfig.getIpPort(getApplicationContext());

            tvGateway.setText(ipPort);

            if (AppConfig.getSDDLService(getApplicationContext())) {
                tvSDDLService.setText(getString(R.string.yes));
            } else {
                tvSDDLService.setText(getString(R.string.no));
            }

            if (AppConfig.getIsSDDLConnected(getApplicationContext())) {
                tvSDDLConnected.setText(getString(R.string.yes));
            } else {
                tvSDDLConnected.setText(getString(R.string.no));
            }

            if (AppConfig.getOBD2Service(getApplicationContext())) {
                tvOBD2Service.setText(getString(R.string.yes));
            } else {
                tvOBD2Service.setText(getString(R.string.no));
            }

            while (MessageManager.hasPendingResponses()) {
                String response = MessageManager.consumeResponse();

                String previousText = tvResponse.getText().toString();

                tvResponse.setText(response);
                tvResponse.append("\n");
                tvResponse.append(previousText);
            }
        }
    };

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            Log.d(TAG, className.toString() + " service is bound");
            ObdGatewayService.ObdGatewayServiceBinder obdGatewayServiceBinder = (ObdGatewayService.ObdGatewayServiceBinder) binder;
            service = obdGatewayServiceBinder.getService();
            service.setContext(MainActivity.this);
            Log.d(TAG, "Starting the live data");
            service.startService();
            isOBD2ServiceBound = true;
        }

        // This method is *only* called when the connection to the service is lost unexpectedly
        // and *not* when the client unbinds (http://developer.android.com/guide/components/bound-services.html)
        // So the isOBD2ServiceBound attribute should also be set to false when we unbind from the service.
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(TAG, className.toString() + " service is unbound");
            isOBD2ServiceBound = false;
        }
    };

    private final Runnable mQueueCommands = new Runnable() {
        public void run() {
            if (service != null && service.isRunning()) {
                queueCommands();
            }
            // run again (polling the OBD)
            mHandler.postDelayed(mQueueCommands, 500); // 500 ms
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnStartService = (Button) findViewById(R.id.btnStartService);
        btnStopService = (Button) findViewById((R.id.btnStopService));

        tvGateway = (TextView) findViewById(R.id.tvGateway);
        tvSDDLService = (TextView) findViewById(R.id.tvSDDLService);
        tvSDDLConnected = (TextView) findViewById(R.id.tvSDDLConnected);
        tvOBD2Service = (TextView) findViewById(R.id.tvOBD2Service);

        tvResponse = (TextView) findViewById(R.id.tvResponse);

        rpmView = (TextView) findViewById(R.id.rpm_read);
        speedView = (TextView) findViewById(R.id.speed_read);

        serviceIntent = new Intent(this, ConnectionService.class);

        btnStartService.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i(TAG, "Click start service");

                if (!AppConfig.hasIp(getApplicationContext())) {
                    Toast.makeText(getApplicationContext(), getString(R.string.noIpPortSet), Toast.LENGTH_LONG).show();
                    return;
                }

                if (!AppConfig.hasBluetoothAddress(getApplicationContext())) {
                    Toast.makeText(getApplicationContext(), getString(R.string.noBluetoothAddressSet), Toast.LENGTH_LONG).show();
                    return;
                }

                startService(serviceIntent);

                doBindService();
            }
        });

        btnStopService.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i(TAG, "Click stop service");

                stopService(serviceIntent);

                doUnbindService();
            }
        });

    }

    public String createMessageString(String rpm, String speed, String temperature) {

        UUID sender = AppConfig.getUuid(getApplicationContext());
        String uuid = sender.toString();

        JSONObject obj = new JSONObject();
        JSONObject obj_info = new JSONObject();

        // Remove unit from the returned OBD data
        String[] rpm_parts = rpm.split(" ");
        int rpm_num = Integer.parseInt(rpm_parts[0]);
        int speed_num = Integer.parseInt(speed.replace("km/h", ""));
        int temperature_num = Integer.parseInt(temperature.replace("C", ""));

        // Less sensibility to the device knobs
        rpm_num = rpm_num/3;
        speed_num = speed_num/2;

        // JSON data
        obj.put("operation", "car_update");
        obj.put("uuid", uuid);
        obj.put("t", System.currentTimeMillis() );
        obj.put("p", counter++);
        obj.put("y", -22.978402);
        obj.put("x", -43.230270);
        obj.put("r", rpm_num);
        obj.put("s", speed_num);
        // TODO Map temperature
//        obj_info.put("r", rpm_num);
//        obj_info.put("s", speed_num);
//        obj_info.put("temperature", temperature_num);
//        obj.put("info", obj_info);


        rpmView.setText(rpm_num + " RPM");
        speedView.setText(speed_num + " km/h");

        Log.i(TAG, "Message sent: " + obj.toString());

        return (obj.toString());
    }

    public void sendMessage(String messageString) {

        UUID sender = AppConfig.getUuid(getApplicationContext());

        ApplicationMessage applicationMessage = new ApplicationMessage();
        applicationMessage.setContentObject(messageString);
        applicationMessage.setPayloadType(ClientLibProtocol.PayloadSerialization.JSON);
        applicationMessage.setTagList(new ArrayList<String>());
        applicationMessage.setSenderID(sender);

        MessageManager.addMessage(applicationMessage);
    }

    private String rpmRead = "";
    private String speedRead = "";
    private String temperatureRead = "";

    public void stateUpdate(final ObdCommandJob job) {
        final String commandName = job.getCommand().getName();
        final String commandResult = job.getCommand().getFormattedResult();

        String commandType = "";
        String readValue = "";

        if (AvailableCommandNames.ENGINE_RPM.getValue().equals(commandName)) {
            rpmRead = commandResult;
        } else if (AvailableCommandNames.SPEED.getValue().equals(commandName)) {
            speedRead = commandResult;
        } else if (AvailableCommandNames.ENGINE_COOLANT_TEMP.getValue().equals(commandName)) {
            temperatureRead = commandResult;
        }

        if (!rpmRead.isEmpty() && !speedRead.isEmpty() && !temperatureRead.isEmpty()) {
            String messageString = createMessageString(rpmRead, speedRead, temperatureRead);

            sendMessage(messageString);

            rpmRead = "";
            speedRead = "";
            temperatureRead = "";
        } else {
            Log.i(TAG, "Failed to obtain all PIDs in this iteraction");
        }
    }

    private int turn = 0; // to skip and collect only one PID per run

    private void queueCommands() {
        Log.d(TAG, "Queueing commands. Service bound? " + isOBD2ServiceBound);
        if (isOBD2ServiceBound) {
            Log.i(TAG, "OBD is bound");
            if (turn == 0) {
                Log.i(TAG, "Getting RPM");
                final ObdCommandJob rpm = new ObdCommandJob(new EngineRPMObdCommand());
                service.queueJob(rpm);
                turn = 1;
            } else if (turn == 1) {
                Log.i(TAG, "Getting SPEED");
                final ObdCommandJob speed = new ObdCommandJob(new SpeedObdCommand());
                service.queueJob(speed);
                turn = 2;
            } else if (turn == 2) {
                Log.i(TAG, "Getting TEMPERATURE");
                final ObdCommandJob temperature = new ObdCommandJob(new EngineCoolantTemperatureObdCommand());
                service.queueJob(temperature);
                turn = 0;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                Intent intentSettings = new Intent(this, SettingsActivity.class);
                startActivityForResult(intentSettings, CONFIGURE_IP_PORT);
                break;
            case R.id.action_bluetooth:
                Intent intentBluetooth = new Intent(this, BluetoothConfig.class);
                startActivityForResult(intentBluetooth, CONFIG_BLUETOOTH);
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CONFIGURE_IP_PORT) {

            if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(), getString(R.string.ipPortSaved), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == CONFIG_BLUETOOTH) {
            if (resultCode == RESULT_OK) {
                String bluetoothAddress = data.getStringExtra(MainActivity.BLUETOOTH_ADDRESS);
                AppConfig.setBluetoothAddress(getApplicationContext(), bluetoothAddress);
                Toast.makeText(getApplicationContext(), "Bluetooth address saved", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        startTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTimer();
    }

    @Override
    protected void onPause() {
        stopTimer();
        super.onPause();
    }

    @Override
    protected void onStop() {
        stopTimer();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isOBD2ServiceBound) {
            doUnbindService();
        }
    }

    /**
     * Start the timer to update the UI
     */
    private void startTimer() {
        uiTimerTask = new TimerTask() {
            public void run() {
                updateUI();
            }
        };

        uiTimer = new Timer();
        uiTimer.schedule(uiTimerTask, 0, screenRefreshRate);
    }

    /**
     * Stop the timer to update the UI
     */
    private void stopTimer() {
        uiTimer.cancel();
    }

    /**
     * Update the User Interface
     */
    private void updateUI() {
        uiHandler.post(uiRunnable);
    }


    private void doBindService() {
        if (!isOBD2ServiceBound) {
            Log.d(TAG, "Binding OBD service. Pre requisites? " + preRequisites);

            if (preRequisites) {
                Intent serviceIntent = new Intent(this, ObdGatewayService.class);
                bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);

                mHandler.post(mQueueCommands);
            } else {
                Log.d(TAG, "Missing pre requisites");
                Toast.makeText(getApplicationContext(), "Something is missing", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void doUnbindService() {
        Log.d(TAG, "Unbinding OBD service");
        if (isOBD2ServiceBound) {
            if (service != null && service.isRunning()) {
                service.stopService();
            }
            unbindService(serviceConnection);
            isOBD2ServiceBound = false;
        }

        if (mHandler != null) {
            mHandler.removeCallbacks(mQueueCommands);
        }

    }
}
