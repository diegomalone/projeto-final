package br.rio.puc.lac.sddlclient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import br.rio.puc.lac.sddlclient.config.AppConfig;

/**
 * Created by Malone on 9/22/2014 12:49 PM
 */


public class SettingsActivity extends Activity {

    private Button btnSave, btnCancel;
    private EditText etIpPort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ip);

        btnSave = (Button) findViewById(R.id.btnIpSave);
        btnCancel = (Button) findViewById(R.id.btnIpCancel);

        etIpPort = (EditText) findViewById(R.id.etIpPort);

        // Show the actual IP/port, if it is valid
        String ipPort = AppConfig.getIpPort(getApplicationContext());
        etIpPort.setText(ipPort);

        btnSave.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                String ipPort = etIpPort.getText().toString();

                // Save the IP/port to the SharedPreferences
                AppConfig.saveIpPort(getApplicationContext(), ipPort);

                Intent returnIntent = new Intent();
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });

        btnCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(RESULT_CANCELED, returnIntent);
                finish();
            }
        });
    }
}
