package br.rio.puc.lac.sddlclient.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

import br.rio.puc.lac.sddlclient.config.AppConfig;
import br.rio.puc.lac.sddlclient.message.MessageManager;
import lac.cnclib.net.NodeConnection;
import lac.cnclib.net.mrudp.MrUdpNodeConnection;

/**
 * Created by Malone on 9/22/2014 1:34 PM
 */
public class ConnectionService extends Service {

    private final String TAG = "ConnectionService";

    private NodeConnection connection;
    private ConnectionListener listener;

    // The connection thread
    private Thread thread;
    private volatile Boolean keepRunning = true;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.v(TAG, "onCreate");

        listener = new ConnectionListener(getApplicationContext());

        thread = new Thread(new Runnable() {

            @Override
            public void run() {

                try {
                    Log.v(TAG, "create mr-udp connection");
                    // Create a new MR-UDP connection
                    connection = new MrUdpNodeConnection();
                    connection.addNodeConnectionListener(listener);

                    Log.v(TAG, "MR-UDP connection created");
                    // Get the gateway IP and port
                    String ip = AppConfig.getIp(getApplicationContext());
                    int port = AppConfig.getPort(getApplicationContext());

                    // Create the socket connection and connect
                    Log.v(TAG, "Creating socket");
                    SocketAddress socket = new InetSocketAddress(ip, port);
                    Log.v(TAG, "Connecting to socket");
                    connection.connect(socket);

                    AppConfig.setWaitingToConnectSDDL(true);

                    Log.v(TAG, "Entering main loop");
                    while (keepRunning) {

                        // Only if it has successfully connected once
                        if (!AppConfig.getWaitingToConnectSDDL()) {

                            // Request stop
                            if (thread.isInterrupted()) {
                                Log.v(TAG, "Thread stoped");
                                break;
                            }

                            if (AppConfig.getIsSDDLConnected(getApplicationContext())) {
                                Log.v(TAG, "Connected");
                                while (MessageManager.hasPendingMessages()) {
                                    connection.sendMessage(MessageManager.consumeMessage());
                                }
                            } else {
                                Log.v(TAG, "Not connected");
                                keepRunning = false;
                                connection.disconnect();
                                stopThread(thread);
                                ConnectionService.this.onDestroy();
                            }
                        } else {
                            Log.v(TAG, "Waiting to connect");
                        }

                        synchronized (thread) {
                            thread.wait(300); // 300 ms
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        // Start the thread
        thread.start();

        // Create the UUID and save it
        if (AppConfig.getUuid(getApplicationContext()) == null) {
            Log.w(TAG, "NULL UUID");
        }
    }

    @Override
    public int onStartCommand(Intent i, int flags, int startId) {
        Log.v(TAG, "onStartCommand");

        AppConfig.setSDDLService(getApplicationContext(), true);

        // Restart service if it got killed
        return START_STICKY;
    }

    @Override
    public void onDestroy() {

        // Set the connection status to false (not connected)
        AppConfig.setIsSDDLConnected(getApplicationContext(), false);

        // Run the thread loop
        synchronized (thread) {
            thread.notify();
        }

        // Set the service status to false (not running)
        AppConfig.setSDDLService(getApplicationContext(), false);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Stop the given thread. Send an interrupt signal
     *
     * @param thread The thread to stop
     */
    private synchronized void stopThread(Thread thread) {
        if (thread != null) {
            thread.interrupt();
        }
    }

    /**
     * Local binder to allow the client to communicate with the service
     */
    public class LocalBinder extends Binder {
        ConnectionService getService() {
            return ConnectionService.this;
        }
    }
}
